// **************
// Animations CSS
// **************
const intro = document.querySelector('.intro-text-box');
intro.classList.add('animated', 'slideInLeft');

const image = document.querySelector('.intro-image');
image.classList.add('animated', 'slideInRight');

// Platform scroll to view
const platform = document.querySelector('.platform-box');

let scroll = () => {

  let view = window.screenY;

  if (view >= 300) {
    platform.classList.add('animated', 'fadeInDown');
  }
}

window.addEventListener('scroll', scroll());

// **************
// Navigation bar
// **************
const btn    = document.querySelector('.btn');
const menu   = document.querySelector('.menu');

btn.addEventListener('click', function() {

  if (btn.classList.contains('is-active')) {
    btn.classList.remove('is-active');
    closeMenu();
  } else {
    btn.classList.add('is-active');
    openMenu();
  }
});

function openMenu() {
  menu.style.display = 'block';
  menu.classList.add('animated', 'flipInX');
  menu.classList.remove('flipOutX');
}

function closeMenu() {

    menu.classList.add('flipOutX');
    menu.classList.remove('flipInX');

    setTimeout(() =>  menu.style.display = 'none', 300)

}

// *****************
// Mail Subscription
// *****************
let formSubmit = document.getElementById('subscribe').onsubmit = (e) => {
  e.preventDefault();
  let email = document.querySelector('.mail').value;

  let subscriberObject = {
    email: email,
    type: 'subscribe',
    target: 'fizioterapeut'
  }

  $.post({
    url: "https://contact.dusanperisic.com",
      method: 'POST',
      data: subscriberObject,
      dataType: 'json'
  });
};


